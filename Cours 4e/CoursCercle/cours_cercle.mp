verbatimtex
%&latex
\documentclass{article}
\usepackage{amsmath,mathrsfs}
\usepackage[T1]{fontenc}
\begin{document}
etex

input geometriesyr16;

u:=0.5cm;

beginfig(1);
figure(0,-1.5u,7u,5.5u)
trace feuillet withcolor blanc;
pair A,B,I;
A=u*(1,1.6);
B=u*(6,2.2);
marque_p:="plein";
pointe(A);
pointe(B);
nomme.lft(A);
nomme.rt(B);
trace segment(A,B);
fin;
endfig;

beginfig(2);
figure(0,-1.5u,7u,5.5u)
trace feuillet withcolor blanc;
pair A,B,I;
A=u*(1,1.6);
B=u*(6,2.2);
marque_p:="plein";
pointe(A);
pointe(B);
nomme.lft(A);
nomme.rt(B);
trace segment(A,B);
I=iso(A,B);
pointe(I);
nomme.lrt(I);
trace codesegments(A,I,I,B,2);
fin;
endfig;

beginfig(3);
figure(0,-1.5u,7u,5.5u)
trace feuillet withcolor blanc;
pair A,B,I,J;
A=u*(1,1.6);
B=u*(6,2.2);
marque_p:="plein";
pointe(A);
pointe(B);
nomme.lft(A);
nomme.rt(B);
trace segment(A,B);
I=iso(A,B);
pointe(I);
nomme.lrt(I);
trace codesegments(A,I,I,B,2);
trace perpendiculaire(A,B,I) withpen pencircle scaled 1pt withcolor violet;
J=rotation(A,I,-90);
trace codeperp(A,I,J,10);
fin;
endfig;

beginfig(4);
figure(0,-1.5u,7u,5.5u)
trace feuillet withcolor blanc;
pair A,B;
A=u*(1,1.6);
B=u*(6,2.2);
marque_p:="plein";
pointe(A);
pointe(B);
nomme.lft(A);
nomme.rt(B);
trace segment(A,B);
fin;
endfig;

beginfig(5);
figure(0,-1.5u,7u,5.5u)
trace feuillet withcolor blanc;
pair A,B;
A=u*(1,1.6);
B=u*(6,2.2);
marque_p:="plein";
pointe(A);
pointe(B);
nomme.lft(A);
nomme.rt(B);
trace segment(A,B);
path cc,cd;
cc=cercles(A,3.7u);
cd=cercles(B,3.7u);
trace cc;
trace cd;
fin;
endfig;

beginfig(6);
figure(0,-1.5u,7u,5.5u)
trace feuillet withcolor blanc;
pair A,B,I,J;
A=u*(1,1.6);
B=u*(6,2.2);
marque_p:="plein";
pointe(A);
pointe(B);
nomme.lft(A);
nomme.rt(B);
trace segment(A,B);
path cc,cd;
cc=cercles(A,3.7u);
cd=cercles(B,3.7u);
trace cc dashed evenly;
trace cd dashed evenly;
trace mediatrice(A,B) withpen pencircle scaled 1pt withcolor violet;
fin;
endfig;


u:=0.7cm;

beginfig(7);
figure(0,-0.5u,7u,6u);
%trace feuillet withcolor blanc;
pair A,B,C,O;
A=u*(1,1);
B=u*(6,2);
C=u*(5,5);
trace triangle(A,B,C);
nomme.llft(A);
nomme.lrt(B);
nomme.top(C);
trace mediatrice(B,C) dashed evenly withcolor violet;
trace mediatrice(A,B) dashed evenly withcolor violet;
trace mediatrice(A,C) dashed evenly withcolor violet;
O=mediatrice(A,B) intersectionpoint mediatrice(B,C);
marque_p:="plein";
pointe(O);
nomme.urt(O);
path cc;
cc=cercles(O,A);
trace cc withpen pencircle scaled 1pt;
%trace compas(O,A,0);
label.urt(btex $\mathscr{C}$ etex,pointarc(cc,80));
fin;
endfig;

u:=1cm;

beginfig(8);
figure(0,-1.5u,7u,5u);
%trace feuillet withcolor blanc;
pair A,B,C,O;
A=u*(1,1);
B=u*(6,2);
path cd;
cd=cercledia(A,B);
C=pointarc(cd,65);
trace triangle(A,B,C);
nomme.llft(A);
nomme.lrt(B);
nomme.top(C);
trace mediatrice(B,C) dashed evenly withcolor violet;
trace mediatrice(A,B) dashed evenly withcolor violet;
trace mediatrice(A,C) dashed evenly withcolor violet;
O=mediatrice(A,B) intersectionpoint mediatrice(B,C);
marque_p:="plein";
pointe(O);
nomme.urt(O);
path cc;
cc=cercles(O,A);
trace cc withpen pencircle scaled 1pt;
%trace compas(O,A,0);
label.urt(btex $\mathscr{C}$ etex,pointarc(cc,100));
trace codeperp(A,C,B,10);
trace codesegments(A,O,O,B,1);
trace segment(O,C);
trace codesegments(A,O,O,C,1);
fin;
endfig;

beginfig(9);
figure(0,-1.5u,7u,5u);
%trace feuillet withcolor blanc;
pair A,B,C,O;
A=u*(1,1);
B=u*(6,2);
path cd;
cd=cercledia(A,B);
C=pointarc(cd,65);
trace triangle(A,B,C);
nomme.llft(A);
nomme.lrt(B);
nomme.top(C);
%trace mediatrice(B,C) dashed evenly withcolor violet;
%trace mediatrice(A,B) dashed evenly withcolor violet;
%trace mediatrice(A,C) dashed evenly withcolor violet;
O=mediatrice(A,B) intersectionpoint mediatrice(B,C);
marque_p:="plein";
pointe(O);
nomme.urt(O);
path cc;
cc=cercles(O,A);
trace cc withpen pencircle scaled 1pt;
%trace compas(O,A,0);
label.urt(btex $\mathscr{C}$ etex,pointarc(cc,100));
trace codeperp(A,C,B,10);
trace codesegments(A,O,O,B,1);
fin;
endfig;

beginfig(10);
figure(0,-1.5u,7u,5u);
%trace feuillet withcolor blanc;
pair A,B,C,O;
A=u*(1,1);
B=u*(6,2);
path cd;
cd=cercledia(A,B);
C=pointarc(cd,65);
trace triangle(A,B,C);
nomme.llft(A);
nomme.lrt(B);
nomme.top(C);
%trace mediatrice(B,C) dashed evenly withcolor violet;
%trace mediatrice(A,B) dashed evenly withcolor violet;
%trace mediatrice(A,C) dashed evenly withcolor violet;
%O=mediatrice(A,B) intersectionpoint mediatrice(B,C);
O=iso(A,B);
marque_p:="plein";
pointe(O);
nomme.bot(O);
path cc;
cc=cercles(O,A);
trace cc withpen pencircle scaled 1pt dashed evenly;
%trace compas(O,A,0);
label.urt(btex $\mathscr{C}$ etex,pointarc(cc,100));
trace codeperp(A,C,B,10);
trace codesegments(A,O,O,B,1);
trace segment(C,O);
trace codesegments(A,O,C,O,1);
fin;
endfig;