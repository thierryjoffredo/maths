verbatimtex
%&latex
\documentclass{article}
%\usepackage{perso-thierry}
\usepackage{amsmath}
\usepackage{fourier}
\begin{document}
etex


input newcourbes;
input couleur;
input geometriesyr16;


beginfig(1);
repere(0,0,-4,4,-4,4,0.6cm,0.6cm);
quadu_xy(rouge);
r_axes;
r_origine;
r_unites;
r_labelxy;
r_pp(3,-2);
r_proj(3,-2,noir);
label.lrt(btex $M$ etex,r_p(3,-2));
r_fin;
endfig;

beginfig(2);
repere(0,0,-4,4,-2.4,2.4,0.6cm,1cm);
quadu_xy(rouge);
r_axes;
r_origine;
r_unites;
r_labelxy;
r_pp(3,-2);
r_proj(3,-2,noir);
r_fin;
label.lrt(btex $M$ etex,r_p(3,-2));
endfig;

beginfig(3);
repere(0,0,-4,4,-4,2,1cm,1cm);
quadu_xy(rouge);
r_axes;
r_origine;
r_unites;
r_labelxy;
r_pp(3,-2);
label.rt(btex $A$ etex,r_p(3,-2));
r_pp(-2,1);
label.lft(btex $B$ etex,r_p(-2,1));
drawarrow r_p(3,-2)--r_p(-2,1) withpen pencircle scaled 1pt;
drawarrow r_p(3,-2)--r_p(-2,-2) dashed evenly withpen pencircle scaled 1pt;
drawarrow r_p(-2,-2)--r_p(-2,1) dashed evenly withpen pencircle scaled 1pt;
label.bot(btex $-5$ etex,r_p(0.5,-2));
label.lft(btex $+3$ etex,r_p(-2,-0.5));
label(btex ${\large \text{Le vecteur }\vec{u}\text{ a pour coordonn\'ees }(-5;3)}$ etex,r_p(0,-3.5)); 
r_fin;
endfig;

beginfig(4);
repere(0,0,-4,4,-5,5,1cm,0.6cm);
quadu_xy(rouge);
r_axes;
r_origine;
r_unites;
r_labelxy;
r_pp(3,4);
label.rt(btex $A$ etex,r_p(3,4));
r_pp(-1,-3);
label.lft(btex $B$ etex,r_p(-1,-3));
drawarrow r_p(3,4)--r_p(-1,-3) withpen pencircle scaled 1pt;
drawarrow r_p(3,4)--r_p(-1,4) dashed evenly withpen pencircle scaled 1pt;
drawarrow r_p(-1,4)--r_p(-1,-3) dashed evenly withpen pencircle scaled 1pt;
label.top(btex $-4$ etex,r_p(1,4));
label.lft(btex $-7$ etex,r_p(-1,0.5));
label(btex ${\large \text{Le vecteur }\vec{u}\text{ a pour coordonn\'ees }(-4;-7)}$ etex,r_p(0,-4.5)); 
r_fin;
endfig;

beginfig(5);
repere(0,0,-4,6,-5,8,1cm,0.6cm);
quadu_xy(rouge);
r_axes;
r_origine;
r_unites;
r_labelxy;
r_pp(4,3);
label.rt(btex $B$ etex,r_p(4,3));
r_pp(-1,-3);
label.lft(btex $A$ etex,r_p(-1,-3));
drawarrow r_p(-1,-3)--r_p(4,3) withpen pencircle scaled 1pt;
drawarrow r_p(4,-3)--r_p(4,3) dashed evenly withpen pencircle scaled 1pt;
drawarrow r_p(-1,-3)--r_p(4,-3) dashed evenly withpen pencircle scaled 1pt;
drawarrow r_p(0,0)--r_p(5,6) withpen pencircle scaled 1pt;
r_proj(5,6,noir);
label.top(btex $+5$ etex,r_p(1.5,-3));
label.lft(btex $+6$ etex,r_p(4,0.5));
label(btex ${\large \text{Le vecteur }\vec{u}\text{ a pour coordonn\'ees }(5;6)}$ etex,r_p(1,-4.5)); 
label.lft(btex $6$ etex,r_p(0,6));
label.bot(btex $5$ etex,r_p(5,0));
r_fin;
endfig;

beginfig(6);
repere(0,0,-4,4,-4,2,1cm,1cm);
quadu_xy(rouge);
r_axes;
r_origine;
r_unites;
r_labelxy;
r_pp(3,-3);
label.rt(btex $A$ etex,r_p(3,-3));
r_pp(-2,1);
label.lft(btex $B$ etex,r_p(-2,1));
draw r_segment(3,-3,-2,1) withpen pencircle scaled 1pt;
r_pp(0.5,-1);
r_proj(0.5,-1,noir);
label.top(btex $\frac{x_A+x_B}{2}$ etex,r_p(0.5,0));
label.lft(btex $\frac{y_A+y_B}{2}$ etex,r_p(0,-1));
label.bot(btex $I$ etex,r_p(0.5,-1));
trace codesegments(r_p(3,-3),r_p(0.5,-1),r_p(0.5,-1),r_p(-2,1),2);
r_fin;
endfig;

beginfig(7);
repere(0,0,-1,7,-0.3,5,1cm,1cm);
%quadu_xy(rouge);
r_axes;
r_origine;
%r_unites;
r_labelxy;
r_pp(6.5,1.2);
label.rt(btex $A$ etex,r_p(6.5,1.2));
r_pp(0.8,4.3);
label.ulft(btex $B$ etex,r_p(0.8,4.3));
draw r_segment(6.5,1.2,0.8,4.3) withpen pencircle scaled 1pt;
draw r_segment(6.5,1.2,6.5,4.3) dashed evenly;
draw r_segment(6.5,4.3,0.8,4.3) dashed evenly;
trace cotation(r_p(6.5,1.2),r_p(6.5,4.3),-2mm,-2mm,btex $y_B-y_A$ etex);
trace cotation(r_p(0.8,4.3),r_p(6.5,4.3),2mm,2mm,btex $x_A-x_B$ etex);
label.llft(btex $H$ etex,r_p(6.5,4.3));
r_proj(6.5,1.2,bleu);
r_proj(0.8,4.3,bleu);
label.bot(btex $x_A$ etex,r_p(6.5,0));
label.bot(btex $x_B$ etex,r_p(0.8,0));
label.lft(btex $y_A$ etex,r_p(0,1.2));
label.lft(btex $y_B$ etex,r_p(0,4.3));
r_fin;
endfig;






